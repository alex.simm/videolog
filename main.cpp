#include <gstreamermm.h>
#include <glibmm/main.h>
#include <gtkmm.h>
#include <filesystem>
#include "Utils.h"
#include "VideoWindow.h"
#include "VideoLogPipeline.h"

using namespace std;
using namespace Gst;
using namespace Glib;

void createDirectory(filesystem::path dir) {
    if (filesystem::exists(dir)) {
        if (!filesystem::is_directory(dir))
            throw runtime_error("Path " + dir.string() + " already exists and is not a directory");
    } else {
        bool created = filesystem::create_directory(dir);
        if (!created)
            throw runtime_error("Directory " + dir.string() + " could not be created");
    }
}

string getMonthYearString() {
    std::time_t time = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
    std::stringstream ss;
    ss << std::put_time(std::localtime(&time), "%b %Y");
    return ss.str();
}

string getDateTimeString() {
    std::time_t time = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
    std::stringstream ss;
    ss << std::put_time(std::localtime(&time), "%d.%m.%Y %R");
    return ss.str();
}

filesystem::path createOutputDirectory(const Configuration &config) {
    string baseDir = config.get("file_output.directory", "");
    if (baseDir.empty())
        throw runtime_error("No output directory specified");

    filesystem::path baseDirPath = filesystem::path(baseDir);
    createDirectory(baseDirPath);

    filesystem::path monthDir = baseDirPath / getMonthYearString();
    createDirectory(monthDir);

    string filename = getDateTimeString();
    if (!config.get("file_output.extension", "").empty()) {
        filename += '.' + config.get("file_output.extension", "");
    }
    filesystem::path outputFile = monthDir / filename;
    if (filesystem::exists(outputFile))
        throw runtime_error("File " + outputFile.string() + " already exists and would be overwritten.");

    return outputFile;
}

/*
 * timeoverlay ohne millis
 * automatically adjust audio volume in microphone (alsasrc) or amplify the signal before encoding
 * hardware acceleration?
 */
int main(int argc, char **argv) {
    Configuration config;

    try {
        // check or create output directory
        filesystem::path outputFile = createOutputDirectory(config);

        // init GTK and gstreamer
        RefPtr<Gtk::Application> app = Gtk::Application::create(argc, argv, "videolog.main");
        Gst::init(argc, argv);

        // create and link pipeline elements
        VideoLogPipeline pipeline(config, outputFile);

        // create and open the video window
        RefPtr<Bus> bus = pipeline.getBus();
        VideoWindow window(bus);
        window.setOnCloseCallback([&pipeline, &app]() {
            pipeline.stop();
            //window.close();
            app->quit();
        });
        window.fullscreen();

        pipeline.start();
        return app->run(window);
    } catch (runtime_error &e) {
        Utils::showError(e.what());
        exit(1);
    }
}
