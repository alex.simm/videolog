#ifndef VIDEOLOG_UTILS_H
#define VIDEOLOG_UTILS_H

#include <string>

class Utils {
public:
    static void showError(std::string message);
};

#endif //VIDEOLOG_UTILS_H
