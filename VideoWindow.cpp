#include "VideoWindow.h"
#include <gstreamermm/xvimagesink.h>
#include <gdk/gdkx.h>
#include <iostream>
#include <utility>

using namespace std;
using namespace Glib;
using namespace Gst;

VideoWindow::VideoWindow(RefPtr<Bus> &bus)
        : bus(bus), onCloseCallback(nullptr), m_vbox(false, 6), windowHandle(-1) {
    add(m_vbox);
    m_vbox.pack_start(m_video_area, Gtk::PACK_EXPAND_WIDGET);
    m_video_area.signal_realize().connect(sigc::mem_fun(*this, &VideoWindow::onVideoAreaRealize));

    // Connect to bus's synchronous message signal to set up the video area for drawing at the appropriate time
    bus->enable_sync_message_emission();
    bus->signal_sync_message().connect(sigc::mem_fun(*this, &VideoWindow::onBusMessageSync));

    // Add a bus watch to receive messages from the pipeline's bus
    busWatchId = bus->add_watch(sigc::mem_fun(*this, &VideoWindow::onBusMessage));

    // add key listener for escape key
    signal_key_press_event().connect(sigc::mem_fun(*this, &VideoWindow::onKeyPress));

    show_all_children();
}

VideoWindow::~VideoWindow() {
    bus->remove_watch(busWatchId);
}

void VideoWindow::setOnCloseCallback(std::function<void()> callback) {
    onCloseCallback = std::move(callback);
}

void VideoWindow::onVideoAreaRealize() {
    /* When the video area (the drawing area) is realized, get its X Window ID and save it for when the
    // video overlay is ready to accept an ID in which to draw the video. */
    windowHandle = GDK_WINDOW_XID (m_video_area.get_window()->gobj());
}

RefPtr<VideoOverlay> VideoWindow::findOverlay(const RefPtr<Element> &element) {
    auto overlay = RefPtr<VideoOverlay>::cast_dynamic(element);
    if (overlay)
        return overlay;

    auto bin = RefPtr<Gst::Bin>::cast_dynamic(element);
    if (!bin)
        return overlay;

    for (auto e : bin->get_children()) {
        overlay = findOverlay(e);
        if (overlay)
            break;
    }

    return overlay;
}

/**
 * Used to receive asynchronous messages from the pipeline's bus, specifically to prepare the video overlay
 * to draw inside the window in which we want it to draw to.
 */
void VideoWindow::onBusMessageSync(const RefPtr<Gst::Message> &message) const {
    if (!gst_is_video_overlay_prepare_window_handle_message(message->gobj()))
        return;

    if (windowHandle != 0) {
        RefPtr<VideoOverlay> videooverlay = findOverlay(RefPtr<Element>::cast_dynamic(message->get_source()));
        if (videooverlay)
            videooverlay->set_window_handle(windowHandle);
    } else {
        std::cerr << "Should have obtained video_window_handle by now!";
    }
}

// This function is used to receive asynchronous messages from play_bin's bus
bool VideoWindow::onBusMessage(const RefPtr<Gst::Bus> &bus, const RefPtr<Gst::Message> &message) {
    switch (message->get_message_type()) {
        case MESSAGE_EOS: {
            stopAndClose();
            break;
        }
        case MESSAGE_ERROR: {
            RefPtr<MessageError> msg_error = RefPtr<MessageError>::cast_static(message);
            if (msg_error) {
                Glib::Error err = msg_error->parse_error();
                cerr << "Error: " << err.what() << endl;
            } else
                cerr << "Error." << endl;

            stopAndClose();
            break;
        }
        default: {
            //std::cout << "debug: on_bus_message: unhandled message=" << G_OBJECT_TYPE_NAME(message->gobj()) << std::endl;
        }
    }

    return true;
}

bool VideoWindow::onKeyPress(GdkEventKey *event) {
    if (event->keyval == GDK_KEY_Escape) {
        stopAndClose();
        return true;
    }

    return false;
}

void VideoWindow::stopAndClose() {
    if (onCloseCallback) {
        onCloseCallback();
    }
}