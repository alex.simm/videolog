#ifndef VIDEOLOG_CONFIGURATION_H
#define VIDEOLOG_CONFIGURATION_H

#include <cstring>
#include <map>

class Configuration {
public:
    /**
     * Loads a configuration from a specific file, or from the default file "./config.list" if the file name
     * is empty.
     */
    explicit Configuration(std::string fileName = "./config.list");

    Configuration(const Configuration &c);

    Configuration &operator=(const Configuration &c);

    ~Configuration() = default;

    std::string get(std::string key, std::string defaultValue = "") const;

    int getInt(std::string key, int defaultValue = -1) const;

    bool getBool(std::string key, bool defaultValue = false) const;

private:
    std::map<std::string,std::string> values;

    void load(const std::string &fileName);
};

#endif //VIDEOLOG_CONFIGURATION_H
