#include "Utils.h"
#include <iostream>
#include <gtkmm.h>

using namespace std;

void Utils::showError(string message) {
    cerr << message << endl;

    Gtk::MessageDialog dialog(message, false, Gtk::MessageType::MESSAGE_ERROR, Gtk::ButtonsType::BUTTONS_OK, true);
    dialog.set_title("Error");
    dialog.set_default_response(Gtk::RESPONSE_YES);
    dialog.run();
}