#ifndef VIDEOLOG_VIDEOLOGPIPELINE_H
#define VIDEOLOG_VIDEOLOGPIPELINE_H

#include <gstreamermm.h>
#include <iostream>
#include <glibmm/main.h>
#include "VideoWindow.h"
#include "Configuration.h"
#include <gtkmm.h>
#include <vector>
#include <filesystem>

class VideoLogPipeline {
public:
    explicit VideoLogPipeline(Configuration &config, std::filesystem::path outputFile);

    Glib::RefPtr<Gst::Bus> getBus() const;

    void start();

    void stop();

private:
    Configuration config;
    Glib::RefPtr<Gst::Pipeline> pipeline;

    void createPipeline(const std::filesystem::path &outputFile);

    //Gst::PadProbeReturn printVideoCaps(const Glib::RefPtr<Gst::Pad> &pad, const Gst::PadProbeInfo &data);

    std::vector<Glib::RefPtr<Gst::Element>> addTextOverlays();

    std::vector<Glib::RefPtr<Gst::Element>> addVideoFilters();

    // camera -> convert -> tee, returns the tee
    Glib::RefPtr<Gst::Element> addVideoElementsRaw();

    // camera -> convert -> tee, returns the tee
    Glib::RefPtr<Gst::Element> addVideoCaptureElementsJPEG();

    // microphone -> convert -> audio-encoder -> queue, return the queue
    Glib::RefPtr<Gst::Element> addAudioEncodeStream();

    static Glib::RefPtr<Gst::Element> createElement(std::string name);

    static Glib::RefPtr<Gst::Element> linkElements(Glib::RefPtr<Gst::Element> previous,
                                                   std::vector<Glib::RefPtr<Gst::Element>> &elements);
};

#endif //VIDEOLOG_VIDEOLOGPIPELINE_H
