#ifndef PLAYER_WINDOW_H_
#define PLAYER_WINDOW_H_

#include <gtkmm.h>
#include <gstreamermm.h>

class VideoWindow : public Gtk::Window {
public:
    VideoWindow(Glib::RefPtr<Gst::Bus> &bus);

    ~VideoWindow() override;

    void setOnCloseCallback(std::function<void()> callback);

private:
    Glib::RefPtr<Gst::Bus> bus;
    std::function<void()> onCloseCallback;

    Gtk::VBox m_vbox;
    Gtk::DrawingArea m_video_area;

    gulong windowHandle;
    guint busWatchId;

    void onVideoAreaRealize();

    bool onBusMessage(const Glib::RefPtr<Gst::Bus> &bus, const Glib::RefPtr<Gst::Message> &message);

    void onBusMessageSync(const Glib::RefPtr<Gst::Message> &message) const;

    bool onKeyPress(GdkEventKey *key_event);

    static Glib::RefPtr<Gst::VideoOverlay> findOverlay(const Glib::RefPtr<Gst::Element> &element);

    void stopAndClose();
};

#endif /* PLAYER_WINDOW_H_ */
