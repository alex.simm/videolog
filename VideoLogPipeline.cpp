#include "VideoLogPipeline.h"
#include "Utils.h"

using namespace std;
using namespace Glib;
using namespace Gst;

VideoLogPipeline::VideoLogPipeline(Configuration &config, filesystem::path outputFile) : config(config) {
    createPipeline(outputFile);
}

Glib::RefPtr<Gst::Bus> VideoLogPipeline::getBus() const {
    return pipeline->get_bus();
}

void VideoLogPipeline::start() {
    pipeline->set_state(Gst::STATE_PLAYING);
}

void VideoLogPipeline::stop() {
    pipeline->set_state(Gst::STATE_NULL);
}

void VideoLogPipeline::createPipeline(const filesystem::path &outputFile) {
    // create pipeline
    pipeline = Pipeline::create();

    // camera -> convert -> tee
    RefPtr<Element> videoTee;
    if (config.getBool("raw_video", false))
        videoTee = addVideoElementsRaw();
    else
        videoTee = addVideoCaptureElementsJPEG();
    //Glib::RefPtr<Gst::Pad> pad = videoTee->get_static_pad("sink");
    //pad->add_probe(Gst::PAD_PROBE_TYPE_BUFFER, sigc::ptr_fun(printVideoCaps));

    // tee -> queue -> display
    RefPtr<Element> videoSinkQueue = createElement("queue");
    RefPtr<Element> videoSink = createElement("xvimagesink");
    pipeline->add(videoSinkQueue)->add(videoSink);
    videoTee->link(videoSinkQueue)->link(videoSink);

    // tee -> video-encoder -> queue
    RefPtr<Element> videoEncode = createElement(config.get("file_output.video_encoder", "theoraenc"));
    RefPtr<Element> videoMuxQueue = createElement("queue");
    pipeline->add(videoEncode)->add(videoMuxQueue);
    videoTee->link(videoEncode)->link(videoMuxQueue);

    // tee -> audio-encoder -> queue
    RefPtr<Element> audioMuxQueue = addAudioEncodeStream();

    // muxer-queues -> muxer -> file-sink
    RefPtr<Element> muxer = createElement(config.get("file_output.muxer", "matroskamux"));
    RefPtr<FileSink> fileSink = RefPtr<FileSink>::cast_dynamic(createElement("filesink"));
    fileSink->set_property("location", outputFile.string());
    fileSink->set_property("buffer_mode", FileSinkBufferMode::FILE_SINK_BUFFER_MODE_UNBUFFERED);
    //fileSink->set_property("o-sync", true);
    pipeline->add(muxer)->add(fileSink);
    videoMuxQueue->link(muxer);
    audioMuxQueue->link(muxer)->link(fileSink);
}

/*Gst::PadProbeReturn VideoLogPipeline::printVideoCaps(const Glib::RefPtr<Gst::Pad> &pad, const Gst::PadProbeInfo &data) {
    Glib::RefPtr<Gst::Caps> caps = pad->get_current_caps();
    cout << caps->to_string() << endl;
    return Gst::PAD_PROBE_OK;
}*/

vector<RefPtr<Element>> VideoLogPipeline::addTextOverlays() {
    vector<RefPtr<Element>> overlays;

    uint deltaY = 0;
    if (config.getBool("video.overlays.add_clock", true)) {
        RefPtr<ClockOverlay> clockOverlay = RefPtr<ClockOverlay>::cast_dynamic(createElement("clockoverlay"));
        clockOverlay->set_property("valignment", Gtk::POS_TOP);
        clockOverlay->set_property("halignment", 2);
        clockOverlay->set_property("time-format", (ustring) "%d.%m.%Y %H:%m:%S");
        clockOverlay->set_property("draw-shadow", true);
        clockOverlay->set_property("draw-outline", false);
        clockOverlay->set_property("font-desc", (ustring) "Sans, 12");
        pipeline->add(clockOverlay);

        overlays.push_back(clockOverlay);

        deltaY += 40;
    }

    if (config.getBool("video.overlays.add_video_time", true)) {
        RefPtr<TimeOverlay> timeOverlay = RefPtr<TimeOverlay>::cast_dynamic(createElement("timeoverlay"));
        timeOverlay->set_property("valignment", Gtk::POS_TOP);
        timeOverlay->set_property("halignment", 2);
        timeOverlay->set_property("deltay", deltaY);
        timeOverlay->set_property("draw-shadow", true);
        timeOverlay->set_property("draw-outline", false);
        timeOverlay->set_property("font-desc", (ustring) "Sans, 12");
        pipeline->add(timeOverlay);

        overlays.push_back(timeOverlay);
    }

    return overlays;
}

vector<RefPtr<Element>> VideoLogPipeline::addVideoFilters() {
    vector<RefPtr<Element>> filters;

    if (config.getBool("video.filters.flip_horizontally", true)) {
        RefPtr<Element> flip = createElement("videoflip");
        flip->set_property("method", 4);
        pipeline->add(flip);
        filters.push_back(flip);
    }

    return filters;
}

/**
 * Adds camera -> convert -> tee to the pipeline and returns the tee.
 */
RefPtr<Element> VideoLogPipeline::addVideoElementsRaw() {
    RefPtr<Caps> caps = Caps::create_simple("video/x-raw")->create_writable();
    caps->set_value("width", 640);
    caps->set_value("height", 480);
    caps->set_value("framerate", Fraction(10, 1));

    // camera -> convert -> tee -> queue -> display
    RefPtr<Element> videoSource = createElement("v4l2src");
    RefPtr<Element> videoConvert = createElement("videoconvert");
    RefPtr<Element> videoTee = createElement("tee");
    pipeline->add(videoSource)->add(videoConvert)->add(videoTee);
    videoSource->link(videoConvert, caps)->link(videoTee);

    return videoTee;
}

/**
 * Adds camera -> convert -> tee to the pipeline and returns the tee.
 */
RefPtr<Element> VideoLogPipeline::addVideoCaptureElementsJPEG() {
    RefPtr<Caps> caps = Caps::create_simple("image/jpeg")->create_writable();
    caps->set_value("width", config.getInt("video.width", 1280));
    caps->set_value("height", config.getInt("video.height", 720));
    caps->set_value("framerate", Fraction(config.getInt("video.rate", 30), 1));

    // camera -> decode -> filters -> overlays -> tee
    RefPtr<Element> videoSource = createElement("v4l2src");
    RefPtr<Element> jpegDecoder = createElement("jpegdec");
    vector<RefPtr<Element>> textOverlays = addTextOverlays();
    vector<RefPtr<Element>> videoFilters = addVideoFilters();
    RefPtr<Element> videoTee = createElement("tee");
    pipeline->add(videoSource)->add(jpegDecoder)->add(videoTee);

    videoSource->link(jpegDecoder, caps);
    RefPtr<Element> previous = linkElements(jpegDecoder, videoFilters);
    previous = linkElements(previous, textOverlays);
    previous->link(videoTee);

    return videoTee;
}

/**
 * Adds microphone -> convert -> audio-encoder -> queue and returns the queue.
 */
RefPtr<Element> VideoLogPipeline::addAudioEncodeStream() {
    RefPtr<Element> audioSource = createElement("alsasrc");
    RefPtr<Element> audioConvert = createElement("audioconvert");
    RefPtr<Element> audioEncoder = createElement(config.get("file_output.audio_encoder", "opusenc"));
    RefPtr<Element> audioMuxQueue = createElement("queue");
    pipeline->add(audioSource)->add(audioConvert)->add(audioEncoder)->add(audioMuxQueue);
    audioSource->link(audioConvert)->link(audioEncoder)->link(audioMuxQueue);

    return audioMuxQueue;
}

RefPtr<Element> VideoLogPipeline::linkElements(RefPtr<Element> previous, vector<RefPtr<Element>> &elements) {
    for (RefPtr<Element> &e : elements) {
        previous->link(e);
        previous = e;
    }

    return previous;
}

RefPtr<Element> VideoLogPipeline::createElement(string name) {
    RefPtr<Element> elem;
    try {
        elem = ElementFactory::create_element(name);
        if (!elem) {
            throw runtime_error("");
        }
    } catch (runtime_error &e) {
        Utils::showError("Could not create pipeline element '" + name + "'");
        exit(0);
    }

    return elem;
}
