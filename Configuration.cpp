#include "Configuration.h"
#include "Utils.h"
#include <stdexcept>
#include <fstream>
#include <sstream>
#include <iostream>

using namespace std;

Configuration::Configuration(string fileName) : values() {
    load(fileName);
}

Configuration::Configuration(const Configuration &c) {
    this->operator=(c);
}

Configuration &Configuration::operator=(const Configuration &c) {
    if (this != &c) {
        this->values = c.values;
    }
    return *this;
}

std::string Configuration::get(string key, string defaultValue) const {
    try {
        return values.at(key);
    } catch (out_of_range &e) {
        return defaultValue;
    }
}

int Configuration::getInt(std::string key, int defaultValue) const {
    try {
        string s = values.at(key);
        return stoi(s);
    } catch (out_of_range &e) {
        return defaultValue;
    } catch (invalid_argument &e) {
        return defaultValue;
    }
}

bool Configuration::getBool(std::string key, bool defaultValue) const {
    try {
        string s = values.at(key);
        return stoi(s) == 1;
    } catch (out_of_range &e) {
        return defaultValue;
    } catch (invalid_argument &e) {
        return defaultValue;
    }
}

void Configuration::load(const std::string &fileName) {
    ifstream input;
    input.exceptions(input.exceptions() | std::ios::failbit);

    try {
        input.open(fileName);

        string line, key, value;
        while (input.good() && getline(input, line)) {
            istringstream lineStream(line);
            if (getline(lineStream, key, '=') && getline(lineStream, value)) {
                values[key] = value;
            }
        }
    }
    catch (const ifstream::failure &e) {
        if (!input.eof()) {
            cerr << "Could not find config file: " << fileName << endl;
        }
    }

    input.close();
}